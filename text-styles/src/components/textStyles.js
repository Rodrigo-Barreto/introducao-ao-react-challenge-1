import React from "react";

class TextStyles extends React.Component {
    constructor(props) {
        super(props);

    }

    state = {
        date: new Date()
    }


    render() {
        const { styles, text } = this.props;
        const { date } = this.state
        return (<>

            <h1 style={styles}>{text}</h1>
            <p >{date.toLocaleTimeString()}</p>
        </>
        );
    }
}

export default TextStyles;